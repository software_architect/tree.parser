## To run the application

Gradle version 5.6.2

1. Run: gradle build or (gradlew | gradlew.bat) build
2. Find jar in: build/libs/
3. Run app: java -jar tree.parser-1.0-SNAPSHOT.jar (absolute or relative path)