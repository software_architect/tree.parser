package utils;

import model.Tree;
import org.apache.commons.collections4.CollectionUtils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

public class Printer {

    public static void print(Tree... trees) {
        if (isEmpty(trees))
            return;
        StringJoiner joiner = new StringJoiner(",");
        List<Tree> nextLine = new LinkedList<>();
        for (Tree tree : trees) {
            joiner.add(tree.getValue());
            nextLine.addAll(tree.getChildren());
        }
        System.out.println(joiner.toString());
        print(nextLine.toArray(new Tree[0]));
    }

    public static void printSecondCase(Tree tree) {
        if (tree == null)
            return;
        getWithChildren(tree).stream().map(Tree::getValue).forEach(System.out::println); // It will print: T04,T06,T24,T03,T31,T20,T18,T05,T29,T11,T08,T33,T02,T44,T14,T20,T45,T06
    }

    private static LinkedList<Tree> getWithChildren(Tree tree) {
        if (CollectionUtils.isNotEmpty(tree.getChildren())) {
            LinkedList<Tree> children = tree.getChildren().stream()
                    .map(Printer::getWithChildren)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toCollection(LinkedList::new));
            children.addFirst(tree);
            return children;
        } else {
            return Stream.of(tree).collect(Collectors.toCollection(LinkedList::new));
        }
    }
}
