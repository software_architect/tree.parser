package assemble;

import model.Tree;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {

    private static final String ROOT_NODE = "Root_Node";
    private static final Pattern PATTERN = Pattern.compile("^(Root_Node|[T]{1}\\d{2}):((?:\\s*[,;]*\\s*[T]{1}\\d{2})+\\s*$)");

    private final int expectedElements;

    public Parser(int expectedElements) {
        this.expectedElements = expectedElements;
    }

    public Tree parse(Path path) throws IOException {

        Tree root = null;
        Map<String, LinkedList<Tree>> all = new HashMap<>(expectedElements);

        for (String line : Files.readAllLines(path)) {
            Matcher matcher = PATTERN.matcher(line);
            if (matcher.find()) {

                String element = matcher.group(1).trim();
                String[] children = matcher.group(2).split("[,;]");

                if (ROOT_NODE.equals(element)) {
                    Tree tree = new Tree(children[0].trim());
                    all.put(tree.getValue(), toList(tree));

                    if (root == null) {
                        root = tree;
                    } else {
                        throw new RuntimeException("More than one root element");
                    }
                } else {
                    Tree tree = Optional.ofNullable(all.get(element))
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .filter(el -> el.childrenCount() < 1)
                            .findFirst()
                            .orElseThrow(() -> new RuntimeException(String.format("Incorrect line order. Element '%s' not found upper the line '%s'", element, line)));

                    for (String child : children) {
                        child = child.trim();
                        Tree newChild = new Tree(child);
                        tree.addChild(newChild);
                        all.merge(child, toList(newChild),
                                (oldVal, newVal) -> {
                                    newVal.addAll(oldVal);
                                    return newVal;
                                });
                    }
                }
            } else {
                throw new RuntimeException("Invalid file format");
            }
        }
        return root;
    }

    private LinkedList<Tree> toList(Tree... elements) {
        return Stream.of(elements).collect(Collectors.toCollection(LinkedList::new));
    }
}
