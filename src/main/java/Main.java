import model.Tree;
import assemble.Parser;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.toIntExact;
import static java.nio.file.Files.*;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static utils.Printer.print;
import static utils.Printer.printSecondCase;

public class Main {

    public static void main(String... args) throws IOException {

//        Path path = Paths.get("/home/oleksandr/IdeaProjects/tree.parser/src/main/resources/valid.txt");

        if (isEmpty(args))
            throw new RuntimeException("No path argument");
        if (args.length > 1)
            throw new RuntimeException("Only one argument is required");

        Path path = Paths.get(args[0]);

        if (!exists(path))
            throw new RuntimeException("File not exist");
        if (!isRegularFile(path))
            throw new RuntimeException("It is not a regular file");

        long linesCount = lines(path).count();
        Tree tree = new Parser(linesCount > MAX_VALUE ? MAX_VALUE : toIntExact(linesCount)).parse(path);
//        print(tree);
        printSecondCase(tree);
    }
}