package model;

import java.util.LinkedList;
import java.util.List;

public class Tree {

    private final String value;
    private final List<Tree> children = new LinkedList<>();

    public Tree(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void addChild(Tree child) {
        this.children.add(child);
    }

    public List<Tree> getChildren() {
        return new LinkedList<>(this.children);
    }

    public int childrenCount() {
        return this.children.size();
    }
}
