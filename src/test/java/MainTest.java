import assemble.Parser;
import model.Tree;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.Printer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.mockito.Matchers.any;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Main.class, Files.class, Printer.class})
public class MainTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void noPathArgument() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("No path argument");

        Main.main();
    }

    @Test
    public void notFile() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("It is not a regular file");

        Main.main("src/test/resources");
    }

    @Test
    public void notExist() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("File not exist");

        Main.main("src/test/resources/notExist.txt");
    }

    @Test
    public void multipleArgsError() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Only one argument is required");

        Main.main("file.txt", "home/file.txt");
    }

    @Test
    public void success() throws Exception {

        Path path = Paths.get("src/test/resources/valid.txt");

        PowerMockito.mockStatic(Files.class);
        PowerMockito.mockStatic(Printer.class);

        Tree tree = Mockito.mock(Tree.class);
        Parser parser = Mockito.mock(Parser.class);
        Stream stream = Mockito.mock(Stream.class);

        Mockito.when(parser.parse(any())).thenReturn(tree);
        Mockito.when(stream.count()).thenReturn(10l);
        PowerMockito.whenNew(Parser.class).withArguments(10).thenReturn(parser);

        PowerMockito.when(Files.exists(any())).thenReturn(true);
        PowerMockito.when(Files.isRegularFile(any())).thenReturn(true);
        PowerMockito.when(Files.lines(any())).thenReturn(stream);

        Main.main("src/test/resources/valid.txt");

        PowerMockito.verifyStatic(Mockito.times(1));
        Files.lines(path);
        PowerMockito.verifyStatic(Mockito.times(1));
        Files.isRegularFile(path);
        PowerMockito.verifyStatic(Mockito.times(1));
        Files.lines(path);
        PowerMockito.verifyStatic(Mockito.times(1));
        Files.lines(path);
        PowerMockito.verifyStatic(Mockito.times(1));
        Printer.print(tree);

        Mockito.verify(parser, Mockito.times(1)).parse(path);
    }
}
