package assemble;

import model.Tree;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {

    private static final String VALID = "src/test/resources/valid.txt";
    private static final String INVALID = "src/test/resources/invalid.txt";
    private static final String INVALID_LINE_ORDER = "src/test/resources/invalid_line_order.txt";
    private static final String INVALID_MORE_THAN_ONE_ROOT_ELEMEN = "src/test/resources/invalid_more_than_one_root_elemen.txt";

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void testValidCase() throws IOException {

        Tree tree = new Parser(25).parse(Paths.get(VALID));

        assertThat(tree).hasFieldOrPropertyWithValue("value", "T04");
        assertThat(tree.getChildren())
                .extracting(Tree::getValue).contains("T06", "T20", "T08", "T33");
        assertThat(tree.getChildren())
                .flatExtracting(Tree::getChildren)
                .extracting(Tree::getValue).contains("T24", "T31", "T18", "T02", "T14", "T20", "T06");
        assertThat(tree.getChildren())
                .flatExtracting(Tree::getChildren)
                .flatExtracting(Tree::getChildren)
                .extracting(Tree::getValue).contains("T03", "T05", "T29", "T44", "T45");
        assertThat(tree.getChildren())
                .flatExtracting(Tree::getChildren)
                .flatExtracting(Tree::getChildren)
                .flatExtracting(Tree::getChildren)
                .extracting(Tree::getValue).contains("T11");
    }

    @Test
    public void testInvalidFileFormatCase() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Invalid file format");

        Tree tree = new Parser(25).parse(Paths.get(INVALID));
    }

    @Test
    public void testInvalidLineOrderCase() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("Incorrect line order. Element 'T29' not found upper the line 'T29: T11'");

        Tree tree = new Parser(25).parse(Paths.get(INVALID_LINE_ORDER));
    }

    @Test
    public void testInvalidMoreThanOneRootElemenCase() throws IOException {

        exceptionRule.expect(RuntimeException.class);
        exceptionRule.expectMessage("More than one root element");

        Tree tree = new Parser(25).parse(Paths.get(INVALID_MORE_THAN_ONE_ROOT_ELEMEN));
    }
}
